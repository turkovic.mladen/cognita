#! python3
import pandas as pd
from pathlib import Path
import os


def get_csv(file_name, force=False):
    """
    :type file_name: str
    :type force: bool
    """

    my_file = Path(file_name+".pkl")
    if (not force) & my_file.is_file():
        # if pkl file exists, read file and return
        return pd.read_pickle(file_name+".pkl")

    # else: # i.e. force == True

    df = pd.read_csv(file_name, header=0, index_col=None, parse_dates=['date'],
                     encoding='utf-8')

    # fix, normalise and cleanup input here

    # save to pickle fast file
    df.to_pickle(file_name+".pkl")
    return df


def get_data(lang_name, data_in, force=False):

    my_file = Path("..\\data\\" + lang_name + "_data.pkl")

    if (not force) & my_file.is_file():

        # if pkl file exists, read file and return
        return pd.read_pickle("..\\data\\" + lang_name + "_data.pkl")

    # Make data split/ pickl if no already present
    mask = data_in.language == lang_name
    df = data_in[mask].copy()

    # save to pickle fast file
    df.to_pickle("..\\data\\" + lang_name + "_data.pkl")

    return df


def print2file(file_out, content):
    file_out.write(content.encode('utf8'))


def save_text(data_in, file_name):

    output_file_path = os.path.join(os.path.dirname(__file__),
                                    "..\\..\\..\\output\\")

    me_file = os.path.join(output_file_path, file_name)
    text_file = open(me_file, "wb")

    for data_out in data_in:
        for data_out2 in data_out:
            print2file(text_file, str(data_out2))
        print2file(text_file, "\n")
