#! python3

from loader import get_csv, get_data, save_text
import analyse
import tf_idf
import summary
import os

input_file_path = os.path.join(os.path.dirname(__file__), "..\\data\\")
file_name = os.path.join(input_file_path, "test_data.csv")

# Load the data

data = get_csv(file_name, False)

# ## 1. Make some initial data analysis.

# output overview of the data (type and languages)
analyse.me(data)

# analyse by date (entries by year-month)
analyse.me_date(data)

# analyse the text properties (length)
analyse.me_texts(data)

# ## 2. make some tests

# make bow
# analyse.me_bow(data)

# ## Make  tf-idf report  ## #

# slice the data or load pickl
dataEn = get_data('Englisch', data, force=False)
dataFr = get_data('Französisch', data, force=False)
dataDe = get_data('Deutsch', data, force=False)

# calculate or load idf dictionary, if exists

# English
print("Processing English ...")
idf_En = tf_idf.get_idf('Englisch', dataEn.text, force=False)
En_tfidf = tf_idf.make_tfidf(dataEn.text, idf_En)
save_text(En_tfidf, "analyse_tfidf_EnText.txt")


# French
print("Processing French ...")
idf_Fr = tf_idf.get_idf('Französisch', dataFr.text, force=False)
Fr_tfidf = tf_idf.make_tfidf(dataFr.text, idf_Fr)
save_text(Fr_tfidf, "analyse_tfidf_FrText.txt")

# German
print("Processing German ...")
idf_De = tf_idf.get_idf('Deutsch', dataDe.text, force=False)
De_tfidf = tf_idf.make_tfidf(dataDe.text, idf_De)
save_text(De_tfidf, "analyse_tfidf_DeText.txt")

# ############################ #

# ## Get summary sub-sentences  ## #
summary_En = summary.get(dataEn.text, En_tfidf, 3, 20)
save_text(summary_En, "summary_EnText.txt")

summary_Fr = summary.get(dataFr.text, Fr_tfidf, 3, 20)
save_text(summary_Fr, "summary_FrText.txt")

summary_De = summary.get(dataDe.text, De_tfidf, 3, 20)
save_text(summary_De, "summary_DeText.txt")

# ############################ #
