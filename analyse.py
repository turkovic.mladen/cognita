#! python3
# Outputs results of overview / analysis
import pandas as pd
from collections import Counter


def me(data):
    print("Analysing the data, print overview ...")
    print("")

    # make data-frame report
    report_df = data.dtypes.to_frame(name='dtypes')

    # print data
    print("data-frame header:")
    print(report_df)

    print("")
    print("overview :")
    print("-----------------")
    print("")
    print(data.groupby('language')['id'].nunique())


def me_date(data):
    print("-----------------")
    print("")
    print(data.groupby(data.date.dt.year)['id'].agg('count'))
    print("-----------------")

    # data_dt = data
    data['year'] = data.date.dt.year
    data['month'] = data.date.dt.month
    data['month_year'] = data[['year', 'month']].apply(lambda x: '-'.join(
        x.map(str)), axis=1)
    print(data.groupby('month_year')['id'].agg('count'))
    print("-----------------")


def me_texts(data):
    # calculate length
    data['len_title'] = data.title.str.len()
    data['len_text'] = data.text.str.len()

    print("")
    print(" TITLE: ")
    print("-----------------")
    # print(data.groupby('len_title')['id'].agg('count'))
    bins = [1, 10, 25, 50, 100]
    groups = data.groupby(pd.cut(data.len_title, bins))
    print(groups.size())
    print("minimum: ", data.len_title.min())
    print("maximum: ", data.len_title.max())
    print("-----------------")

    print("")
    print(" TEXT: ")
    print("-----------------")
    # print(data.groupby('len_text')['id'].agg('count'))
    bins = [1, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000,
            10000, 20000, 30000, 40000]
    groups = data.groupby(pd.cut(data.len_text, bins))
    print(groups.size())
    print("minimum: ", data.len_text.min())
    print("maximum: ", data.len_text.max())
    print("-----------------")


def me_bow(data):
    # make BOW
    data['BOW_title'] = data.title.apply(lambda x: Counter(x.split(" ")).
                                         most_common())
    data['BOW_text'] = data.text.apply(lambda x: Counter(x.split(" ")).
                                       most_common())

    print("Print top 10...")
    print("-----------------")
    print(data.BOW_title[:10])
    print("")
    print("-----------------")
    print(data.BOW_text[:10])
