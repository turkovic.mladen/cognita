#! python3
# Calculate tf-idf

# import sklearn.feature_extraction.text.TfidfVectorizer

from collections import Counter
import math
import operator
from pathlib import Path
import json


def get_idf(lang_name, data_in, force=False):

    my_file = Path("..\\data\\" + lang_name + "_idf.json")

    # if json file exists, read file and return
    if (not force) & my_file.is_file():
        with open("..\\data\\" + lang_name + "_idf.json", 'r') as f:
            idf_data_in = json.load(f)
        return idf_data_in

    # Make idf data / json if no already present
    idf_data_in = idf_calc(data_in)

    # save to json file
    with open("..\\data\\" + lang_name + "_idf.json", 'w') as f:
        json.dump(idf_data_in, f)

    return idf_data_in


def idf_calc(data_txt):
    cnt = data_txt.apply(lambda x: Counter(x.split(" ")))

    all_txt = data_txt.str.cat(sep=' ')
    all_count = Counter(all_txt.split(" ")).most_common()

    total_doc = len(cnt)
    idf_x = {}
    for key, v in all_count:
        if v > 1:
            # count how many records contain key
            cnt_keys = sum(cnt.apply(lambda x: key in x.keys()))
            idf_x[key] = math.log10(total_doc / cnt_keys)
        else:
            idf_x[key] = math.log10(total_doc / 1)

    return idf_x


def tf_calc(row):

    y = Counter(row.split(" "))
    total = sum(y.values(), 0.0)

    for key in y:
        y[key] /= total

    return y.most_common()


def tfidf_calc(x, idf_title):

    tf_idf = {}
    for key, value in x:
        tf_idf[key] = value * idf_title.get(key)

    tf_idf_sort = sorted(tf_idf.items(), key=operator.itemgetter(1),
                         reverse=True)

    return tf_idf_sort


def make_tfidf(data_in, idf_dict):

    in_data = data_in.copy()

    in_data['TF'] = in_data.apply(tf_calc)
    in_data['TFIDF'] = in_data.TF.apply((lambda x: tfidf_calc(x, idf_dict)))

    return in_data.TFIDF
