#! python3

import re


def get(text_in, tfidf_in, top_nr, context_nr):

    summary_s = []

    for idx, ttt in tfidf_in.items():

        tops = ttt[:top_nr]
        txt_ = text_in[idx]
        txt_graph = re.sub('.', '_', txt_)

        summary = "Summary: \n"
        keys = "keys: "

        for key, value in tops:
            # remove issues from the key (use at loading?)
            key = re.sub('[!@#$)(,.?:;"§%&/=\\\]', '', key)

            if len(key) > 1:
                keys += key + "; "
                positions = [m.start() for m in re.finditer(key, txt_)]

                for pos in positions:
                    pos_start = max(pos-context_nr, 0)
                    pos_end = min(pos+context_nr, len(txt_))
                    summary += "\t\t" + txt_[pos_start:pos_end] + "\n"
                    txt_graph = graph_txt(txt_graph, "^", pos_start, pos_end)

        summary_s.append([txt_ + "\n" + txt_graph + "\n\t" + keys + "\n\t",
                          summary])

    return summary_s


def graph_txt(txt_in, char, pos_start, pos_end):
    # convert between start and end to ^
    txt_list = list(txt_in)
    txt_list[pos_start: pos_end] = [char] * (pos_end - pos_start)

    return "".join(txt_list)


def get_copy(text_in, tfidf_in, top_nr):

    summary = []

    for idx, txt in enumerate(text_in):
        txt_tfidf = tfidf_in.get(idx)
        print(txt_tfidf)
        print(str(txt_tfidf))
        tops = txt_tfidf[:top_nr]

        for key, value in tops:
            positions = [m.start() for m in re.finditer(key, txt)]

            for pos in positions:
                pos_start = max(pos-15, 0)
                pos_end = min(pos+15, len(txt))
                summary.append(txt[pos_start:pos_end])

    return summary
